# Skelton NodeJs App

RestAPI - Back-End for NodeJs/ExpessJs 

The following services are available via HTTP API (REST API)

## Getting Started

Seklton & Getting started NodeJS App

## Deployment

This backend is running on nodjs env. please be sure to have the above requirements

## Built With

* [NodeJS]
* [ExpressJs]
* [MongoDB]

## Contributing

Please read for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Abdelhadi Habchi** - *Initial work & FullStack developer* - [AbdelhadiDev](https://abdelhadidev.com)

## License

This project is licensed under the MIT License
